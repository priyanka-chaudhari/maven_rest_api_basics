package com.example.mybatis.springbootmybatis;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {
	
	@RequestMapping(value = "/", method = { RequestMethod.GET })
	public String sample() {
		return "Hello Developer";
	}
}